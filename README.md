# local-gitlab-helm-deployment

## requirements

* minikube
* yq
* helm v3

## start

```bash
make deploy
```

## delete

```bash
make rm
```
