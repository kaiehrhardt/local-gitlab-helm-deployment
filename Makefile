INIT_TOKEN = "suuuper-s3cret-token"
GITLAB_REPO = "gitlab-config"
MINIKUBE_IP = $(shell minikube ip)

# default deployment
test-req:
	@[ $(shell which minikube) ] && echo "minikube OK" || echo "install minikube"
	@[ $(shell which yq) ] && echo "yq OK" || echo "install yq"
	@[ $(shell which helm) ] && echo "helm OK" || echo "install yq"

start-mini:
	minikube start --cpus 4 --memory 8g --addons ingress

helm:
	helm repo add gitlab https://charts.gitlab.io/
	helm repo update
	helm upgrade --install gitlab gitlab/gitlab -f values.yml

fix-ip:
	yq -i '.global.hosts.domain = "$(MINIKUBE_IP).nip.io"' values.yml
	yq -i '.global.hosts.externalIP = "$(MINIKUBE_IP)"' values.yml
	cat values.yml

test:
	curl -Lk gitlab.$(MINIKUBE_IP).nip.io

get-creds:
	@echo user: root
	@kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo

rm:
	minikube delete

deploy: test-req start-mini fix-ip helm get-creds

# mc config -> needed for create and restore backup
set-mc-config:
	mc alias set gitlab \
		https://minio.$(MINIKUBE_IP).nip.io/ \
		$(shell kubectl get secret gitlab-minio-secret -ojsonpath='{.data.accesskey}' | base64 --decode) \
		$(shell kubectl get secret gitlab-minio-secret -ojsonpath='{.data.secretkey}' | base64 --decode) \
		--insecure

# create backup
initial-token:
	kubectl exec -it $(shell kubectl get pods -lapp=toolbox -oname) -- /srv/gitlab/bin/rails runner "token = User.find_by_username('root').personal_access_tokens.create(scopes: [:api], name: 'Automation token'); token.set_token('$(INIT_TOKEN)'); token.save!"

configure-gitlab:
	terraform -chdir=$(GITLAB_REPO) init
	terraform -chdir=$(GITLAB_REPO) apply  -var gitlab_token=$(INIT_TOKEN) -var minikubeip=$(MINIKUBE_IP) -auto-approve

backup-rails-secret:
	kubectl get secrets gitlab-rails-secret -o jsonpath="{.data['secrets\.yml']}" | base64 --decode > gitlab-secrets.yml

create-backup:
	kubectl exec \
		$(shell kubectl get pods -lapp=toolbox --field-selector=status.phase=Running -o=name) \
		-it -- \
		backup-utility

get-backup:
	mc --insecure cp gitlab/gitlab-backups/$(shell mc --insecure ls gitlab/gitlab-backups --json | jq -r .key) .

# restore backup
restore-rails-secret:
	@kubectl delete secrets gitlab-rails-secret
	@kubectl create secret generic gitlab-rails-secret --from-file=secrets.yml=gitlab-secrets.yml
	@kubectl delete pods -lapp=sidekiq
	@kubectl delete pods -lapp=webservice
	@kubectl delete pods -lapp=toolbox

put-backup:
	mc --insecure cp $(shell find . -name "*.tar") gitlab/gitlab-backups/

restore-backup:
	kubectl exec \
		$(shell kubectl get pods -lapp=toolbox --field-selector=status.phase=Running -o=name) \
		-it -- \
		backup-utility --restore -t \
		$(shell mc --insecure ls gitlab/gitlab-backups --json | jq -r .key | sed -e s/_gitlab_backup.tar//)
